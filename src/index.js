import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Carousel from "./Carousel";
import Menu from "./Menu";
import Image from "./Image"
import "antd/dist/antd.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
   <Menu />
   <main>
     <h1>Welcome To MRX Cinemas</h1>
   </main>
   <Carousel />
   <font>
     <h1>Popular This Week</h1>
   </font>
   <outer>
   <Image />
   </outer>
   <footer>
     <h3>Ant Design ©2022 Created by Safira Tyas Wandita</h3>
   </footer>
  </React.StrictMode>
);
