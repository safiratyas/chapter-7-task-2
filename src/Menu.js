import React from "react";
// import style from "./Header.module.css";
import { Menu } from 'antd';
import { HomeOutlined, AppstoreOutlined, DatabaseOutlined, FormOutlined } from '@ant-design/icons';

const App = () => (
  <Menu mode="horizontal" defaultSelectedKeys={['home']}>
    <Menu.Item key="home" icon={<HomeOutlined />}>
      Home
    </Menu.Item>
    <Menu.SubMenu key="sign-up" title="Sign Up" icon={<FormOutlined />}>
    </Menu.SubMenu>
    <Menu.SubMenu key="SubMenu" title="List Of Cinema" icon={<DatabaseOutlined />}>
      <Menu.ItemGroup title="Jabodetabek">
        <Menu.Item key="one" icon={<AppstoreOutlined />}>
        Jakarta
        </Menu.Item>
        <Menu.Item key="two" icon={<AppstoreOutlined />}>
        Bogor
        </Menu.Item>
        <Menu.Item key="three" icon={<AppstoreOutlined />}>
        Depok
        </Menu.Item>
        <Menu.Item key="four" icon={<AppstoreOutlined />}>
        Tanggerang
        </Menu.Item>
        <Menu.Item key="five" icon={<AppstoreOutlined />}>
        Bekasi
        </Menu.Item>
      </Menu.ItemGroup>
      <Menu.ItemGroup title="Di Luar Jabodetabek">
        <Menu.Item key="one" icon={<AppstoreOutlined />}>
        Surabaya
        </Menu.Item>
        <Menu.Item key="two" icon={<AppstoreOutlined />}>
        Yogyakarta
        </Menu.Item>
        <Menu.Item key="three" icon={<AppstoreOutlined />}>
        Malang
        </Menu.Item>
        <Menu.Item key="four" icon={<AppstoreOutlined />}>
        Medan
        </Menu.Item>
        <Menu.Item key="five" icon={<AppstoreOutlined />}>
        Solo
        </Menu.Item>
      </Menu.ItemGroup>
    </Menu.SubMenu>
  </Menu>
);


export default App;