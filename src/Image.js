import { Image } from 'antd';

function ImageDemo() {
  return (
    <Image.PreviewGroup>
    <Image
      width={400}
      src="https://m.media-amazon.com/images/M/MV5BNzBiMTlhNTAtNDc4Yi00M2FiLWE3ODMtNzgwNWI3ZGFhYjNkXkEyXkFqcGdeQXVyMjQ5NjMxNDA@._V1_.jpg"
    />
    <Image
      width={400}
      src="https://m.media-amazon.com/images/M/MV5BMjRkYjkyYzUtZjliNi00ODlmLWE5OTEtZDMzY2UxY2E0MGFkXkEyXkFqcGdeQXVyMTA3MzQ4MTcw._V1_.jpg"
    />
    <Image
      width={400}
      src="https://m.media-amazon.com/images/M/MV5BNWEyZTYzZDYtMzQ0Zi00MjMzLThjMWUtNzYyMzdiZDk0MzBiXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_.jpg"
    />
  </Image.PreviewGroup>
  );
}

export default () => <ImageDemo />; 