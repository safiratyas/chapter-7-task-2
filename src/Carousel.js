import React from "react";
import { Carousel } from 'antd';

const contentStyle = {
  height: '500px',
  width:'250px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

export default () => (
  <Carousel autoplay>
    <div>
      <h3 style={contentStyle}><img src="http://www.brunswickmoviebowl.com/wp-content/uploads/2016/06/movie-header.jpg"></img></h3>
    </div>
    <div>
      <h3 style={contentStyle}><img src="http://www.brunswickmoviebowl.com/wp-content/uploads/2016/06/movie-header.jpg"></img></h3>
    </div>
    <div>
      <h3 style={contentStyle}><img src="http://www.brunswickmoviebowl.com/wp-content/uploads/2016/06/movie-header.jpg"></img></h3>
    </div>
    <div>
      <h3 style={contentStyle}><img src="http://www.brunswickmoviebowl.com/wp-content/uploads/2016/06/movie-header.jpg"></img></h3>
    </div>
  </Carousel>
);